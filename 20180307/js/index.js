$(function(){
	
	$('.btn_china').click(function () {
        var lan = 'zh_CN';
        window.localStorage.setItem('language', lan);
        lang(lan);
        $('.btn_china').addClass('active');
        $('.btn_english').removeClass('active');
    });
    $('.btn_english').click(function () {
        var lan = 'en';
        window.localStorage.setItem('language', lan)
        lang(lan);
        $('.btn_china').removeClass('active');
        $('.btn_english').addClass('active');
    });
    var lan = window.localStorage.getItem('language') ? window.localStorage.getItem('language') : 'zh_CN';
    lang(lan);
	
	touch.on(".vote_rule",'tap',function(){
	 	$("#alert1").css("display","block");
	 });
	touch.on(".alert_button",'tap',function(){
	 	$(".alert").css("display","none");
	 });
//	var tapClick0 = 0,tapClick1 = 0,tapClick2 = 0,tapClick3 = 0,tapClick4 = 0,tapClick5 = 0,tapClick6 = 0,tapClick7 = 0;
//	touch.on(".slide_zan",'tap',function(){
//		var num = swiper.activeIndex;
//		switch(num)
//			{
//			case 0:
//			   zan(++tapClick0); 
//			  break;
//			case 1:
//			   zan(++tapClick1);
//			  break;
//			case 2:
//			   tapClick2 ++;
//			   zan(tapClick2);
//			  break;
//			case 3:
//			   tapClick3 ++;
//			   zan(tapClick3);
//			  break;
//			case 4:
//			   tapClick4 ++;
//			   zan(tapClick4);
//			  break;
//			case 5:
//			   tapClick5 ++;
//			   zan(tapClick5);
//			  break;
//			case 6:
//			   tapClick6 ++;
//			   zan(tapClick6);
//			  break;
//			case 7:
//			   tapClick7 ++;
//			   zan(tapClick7);
//			  break;
//			default:
//			 break;
//			}
//	 }); 
	 
	 
	 var tickep = 0;
	 var a=0,b=0,c=0,d=0,e=0,f=0,g=0,h=0;
	 touch.on(".vote_button",'tap',function(){
		var num = swiper.activeIndex;
		$("#slide"+num).find(".vote_button").css("background","#373737");
		
		switch(num)
			{
			case 0:
			   a ++;
			   if(a==1){
			   	tickep = tickep+1;
			   	tick(tickep); 
			   }else{
			   	tickep =tickep;
			   }
			   
			  break;
			case 1:
			   b ++;
			   if(b==1){
			   	tickep = tickep+1;
			   	tick(tickep); 
			   }else{
			   	tickep =tickep;
			   }
			  break;
			case 2:
			   c ++;
			   if(c==1){
			   	tickep = tickep+1;
			   	tick(tickep); 
			   }else{
			   	tickep =tickep;
			   }
			  break;
			case 3:
			   d ++;
			   if(d==1){
			   	tickep = tickep+1;
			   	tick(tickep); 
			   }else{
			   	tickep =tickep;
			   }
			  break;
			case 4:
			   e ++;
			   if(e==1){
			   	tickep = tickep+1;
			   	tick(tickep); 
			   }else{
			   	tickep =tickep;
			   }
			  break;
			case 5:
			    f ++;
			   if(f==1){
			   	tickep = tickep+1;
			   	tick(tickep); 
			   }else{
			   	tickep =tickep;
			   }
			  break;
			case 6:
			    g ++;
			   if(g==1){
			   	tickep = tickep+1;
			   	tick(tickep); 
			   }else{
			   	tickep =tickep;
			   }
			  break;
			case 7:
			    h ++;
			   if(h==1){
			   	tickep = tickep+1;
			   	tick(tickep); 
			   }else{
			   	tickep =tickep;
			   }
			  break;
			default:
			 break;
			}
	 }); 
	
	
})
//function zan(tapClick){
//	var num_text = $(".slide_num").eq(swiper.activeIndex).text();
//  if(tapClick == 1){
//	 	num_text = parseInt(num_text)+1;
//	 	$(".slide_num").eq(swiper.activeIndex).text(num_text);
//	}else{
//		 num_text = $(".slide_num").eq(swiper.activeIndex).text();
//	}
//}

function tick(tic){
	switch(tic)
		{
		case 1:			   
		   	$("#vote_bar1").css("width","50%");	
		   	break;
		case 2:			   
		   	$("#vote_bar1").css("width","100%");			  
		   break;
		case 3:			   
		   	$("#vote_bar2").css("width","50%");			  
		   break;
		case 4:			    
		   	$("#vote_bar2").css("width","100%");			   
		   break;
		case 5:			    
		   	$("#vote_bar3").css("width","50%");			  
		   break;
		case 6:			    
		   	$("#vote_bar3").css("width","100%");			  
		   break;
		case 7:			    
		   	$("#vote_bar4").css("width","50%");			   
		   break;
		case 8:			    
		   	$("#vote_bar4").css("width","100%");			  
			   break;
			default:
			 break;
			}
}
    function lang(lan) {
        if (lan == 'zh_CN') {
            $('.btn_china').removeClass('checked-language')
            $('.btn_english').addClass('checked-language')
        } else {
            $('.btn_china').removeClass('checked-language')
            $('.btn_english').addClass('checked-language')
        }
        jQuery.i18n.properties({
            name: 'lan',
            mode: 'map',
            cache: true,
            async: true,
            language: lan,
            path: './language/',
            callback: function () {
                $('.login_title').text($.i18n.prop('login.title'))
                $('.login_lang h3').text($.i18n.prop('login.lang'))

                $('label[for="u_id"]').html($.i18n.prop('login.u_id'))
                $('label[for="u_name"]').html($.i18n.prop('login.u_name'))
                $('label[for="ID"]').html($.i18n.prop('login.ID'))
                
               
            }
        })
    }
