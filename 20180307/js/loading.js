var canvas = document.getElementById("loading_canvas");
var stage = new createjs.Stage(canvas);

var manifest;
var preload;
var progressText = new createjs.Text("", "16px Arial", "#5d0101");
progressText.x = 30-progressText.getMeasuredWidth() / 2;
progressText.y = 50;
stage.addChild(progressText);
stage.update();

//定义相关JSON格式文件列表
function setupManifest() {
    manifest = [
            
	         {src:  "img/loading.png", id: "p1_1"},
	         {src:  "img/loading_1.png", id: "p1_2"},
	         {src:  "img/loading_bottom.png", id: "p1_3"}
    ];
}

//开始预加载
function startPreload() {
    preload = new createjs.LoadQueue(true);
    //注意加载音频文件需要调用如下代码行
    preload.installPlugin(createjs.Sound);         
    preload.on("fileload", handleFileLoad);
    preload.on("progress", handleFileProgress);
    preload.on("complete", loadComplete);
    preload.on("error", loadError);
    preload.loadManifest(manifest);
 
}

//处理单个文件加载
function handleFileLoad(event) {
    console.log("文件类型: " + event.item.type);
    if(event.item.id == "logo"){
        console.log("logo图片已成功加载");
    }
}
 
//处理加载错误：大家可以修改成错误的文件地址，可在控制台看到此方法调用
function loadError(evt) {
    console.log("加载出错！",evt.text);
}
 
//已加载完毕进度 
function handleFileProgress(event) {
     progressText.text = (preload.progress*100|0) + "%";
    $("#progress-bar").css("width",progressText.text );
   
    stage.update();
    
    
}

//全度资源加载完毕
function loadComplete(event) {
  console.log("已加载完毕全部资源");  
	$("#loading").css("display","none");		
	
}

setupManifest();
startPreload();