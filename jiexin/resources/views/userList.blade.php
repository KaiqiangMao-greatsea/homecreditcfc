<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>捷信简易管理后台</title>
    <script src="{{asset('/js/app.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <!-- Fonts -->

    <!-- Styles -->
    <style>
        .border {
            border: #5e5d5d solid 1px;
            margin: 5px;
        }

        .padding {
            padding: 5px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>用户角色列表</h1>
    <div class="row">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                {{--<th>类型</th>--}}
                <th>部门</th>
                <th>员工号</th>
                <th>中文名</th>
                <th>英文名</th>
                <th>护照/身份证</th>
                {{--<th>职位</th>--}}
                {{--<th>级别</th>--}}
                {{--<th>手机号</th>--}}
                {{--<th>邮箱</th>--}}
                {{--<th>角色</th>--}}
                <th>状态（是否认证过）</th>
                {{--<th></th>--}}

            </tr>
            </thead>
            <tbody>
            @foreach($data as $k=>$v)
                <tr>
                    <th scope="row">{{$k+1}}</th>
                    {{--<td>{{$v['type']}}</td>--}}
                    <td>{{$v['department']}}</td>
                    <td>{{$v['e_id']}}</td>
                    <td>{{$v['c_name']}}</td>
                    <td>{{$v['e_name']}}</td>
                    <td>{{$v['id_number']}}</td>
                    {{--<td>{{$v['title']}}</td>--}}
                    {{--<td>{{$v['level']}}</td>--}}
                    {{--<td>{{$v['phone']}}</td>--}}
                    {{--                    <td>{{$v['email']}}</td>--}}
                    {{--<td>--}}
                        {{--@if($v['qiye'])--}}
                            {{--企业--}}
                        {{--@endif--}}

                        {{--@if($v['jieguo'])--}}
                            {{--结果--}}
                        {{--@endif--}}

                        {{--@if($v['chuangxin'])--}}
                            {{--创新--}}
                        {{--@endif--}}

                        {{--@if($v['gongping'])--}}
                            {{--公平--}}
                        {{--@endif--}}
                    {{--</td>--}}
                    <td>
                        @if($v['status'])

                        @else

                        @endif
                    </td>
                    {{--<th></th>--}}

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
</script>
</body>
</html>
