<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>捷信简易管理后台</title>
    <script src="{{asset('/js/app.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">

    <!-- CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->

    <!-- Styles -->
    <style>
        .border {
            border: #5e5d5d solid 1px;
            margin: 5px;
        }

        .padding {
            padding: 5px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>关键词列表</h1>
    <div class="row">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>企业家精神</th>
                <th>结果导向</th>
                <th>创新精神</th>
                <th>公平公正</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data[1] as $k=>$v)
                <tr>
                    <th scope="row">{{$k+1}}</th>

                    <td>{{ $v['id'].$v['c_name'].' '.$v['e_name'] }}</td>
                    <td>{{ $data[2][$k]['id'].$data[2][$k]['c_name'].' '.$data[2][$k]['e_name'] }}</td>
                    <td>{{ $data[3][$k]['id'].$data[3][$k]['c_name'].' '.$data[3][$k]['e_name'] }}</td>
                    <td>{{ $data[4][$k]['id'].$data[4][$k]['c_name'].' '.$data[4][$k]['e_name'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    {{--$.ajaxSetup({--}}
        {{--headers: {--}}
            {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--}--}}
    {{--});--}}
    {{--$('.banner').change(function (e) {--}}
        {{--console.log(e)--}}
        {{--let name = e.target.name;--}}
        {{--let file = e.target.files[0];--}}
        {{--let formFile = new FormData();--}}
        {{--formFile.append(name, file);--}}
        {{--$.ajax({--}}
            {{--url: '{{ route('valueBannerAdd') }}?name=' + name,--}}
            {{--type: 'POST',--}}
            {{--data: formFile,--}}
            {{--dataType: "json",--}}
            {{--cache: false,//上传文件无需缓存--}}
            {{--processData: false,//用于对data参数进行序列化处理 这里必须false--}}
            {{--contentType: false, //必须--}}
            {{--success: function (res) {--}}
                {{--if (res.banner) {--}}
                    {{--let str = `<img height="200px" src="${res.banner}" alt="">`--}}
                    {{--$('#' + name).html(str);--}}
                {{--} else {--}}
                    {{--$('#' + name).children()[0].val('')--}}
                {{--}--}}
            {{--},--}}
        {{--})--}}
    {{--})--}}
</script>
</body>
</html>
