<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>捷信简易管理后台</title>
    <script src="{{asset('/js/app.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->

    <!-- Styles -->
    <style>
        .border {
            border: #5e5d5d solid 1px;
            margin: 5px;
        }

        .padding {
            padding: 5px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>项目开关</h1>
    <h2 style="color: red;">项目正式上线时间03月28日06点，项目功能开关点击时请确认当前活动状态</h2>
    <div class="row">

        <div class="col-lg-5 border">
            <p>{{$switcher->keywordStatus()}}</p>
            <p>关键词投票管理</p>
            <a class="padding" href="{{ route('switch') }}?type=keyword&action=1">开始</a>
            <a class="padding" href="{{ route('switch') }}?type=keyword&action=2">结束</a>
            <a class="padding" href="{{ route('switch') }}?type=keyword&action=0">关闭</a>
        </div>

        <div class="col-lg-5 border">
            <p>{{$switcher->valueStatus()}}</p>
            <p>价值观投票管理</p>
            <a class="padding" href="{{ route('switch') }}?type=value&action=1">开始</a>
            <a class="padding" href="{{ route('switch') }}?type=value&action=2">结束</a>
            <a class="padding" href="{{ route('switch') }}?type=value&action=0">关闭</a>
        </div>
    </div>



    <h1>报表导出</h1>
    <div class="row">

        <div class="col-lg-5 border">
            <p> 操作提示：</p>
            <table class="table table-hover">
                <tr>
                    <th>名称</th>
                    <th>下载链接</th>
                </tr>
                <tr>
                    <th>价值观之星投票排名</th>
                    <th><a href="{{route('excelValueRanks')}}">下载链接</a></th>
                </tr>
                <tr>
                    <th>候选人获得的总票数</th>
                    <th><a href="{{route('excelValueCount')}}">下载链接</a></th>
                </tr>
                <tr>
                    <th>用户投票详情</th>
                    <th><a href="{{route('excelUserValueInfo')}}">下载链接</a></th>
                </tr>
                <tr>
                    <th>价值观词云投票排名</th>
                    <th><a href="{{route('excelKeywordRanks')}}">下载链接</a></th>
                </tr>

                <tr>
                    <th>价值观词云的总票数</th>
                    <th><a href="{{route('excelKeywordsInfo')}}">下载链接</a></th>
                </tr>
                <tr>
                    <th>用户选择的词云数据</th>
                    <th><a href="{{route('excelValuesInfo')}}">下载链接</a></th>
                </tr>
            </table>
            <p>警告：请不要在活动高峰期进行操作（高峰期：参赛人员正在进行大批量投票）</p>
        </div>
    </div>
    <h1>人员管理</h1>
    <div class="row">

        <div class="col-lg-5 border">

            @if($user->count()>0)
                {{--@if(0)--}}
                <a href="{{ route('userList') }}">用户列表</a>

            @else
                <p>所有用户上传</p>
                <form action="{{ route('userAdd') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <label for="userInput">点击上传</label>
                    <input style="display: none;" type="file" value="" name="userlist" id="userInput"
                           {{--accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">--}}
                           accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                    <span id="fileName"></span>
                    <br>
                    <input type="submit" name="submit" value="上传">
                </form>
            @endif
            <br>
            <br>
            {{--<a href="{{ route('userReset') }}" style="color: red;">重置用户表</a>--}}
        </div>

        <div class="col-lg-5 border">
            @if($valueUser->count()>0)
                {{--@if(0)--}}
                <a href="{{ route('valueUserList') }}">参赛用户 {{ $valueUser->count() }} </a>
            @else
                <p>参赛用户上传</p>
                <form action="{{ route('valueUserAdd') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <label for="userInput">点击上传</label>
                    <input style="display: none;" type="file" value="" name="userlist" id="userInput"
                           {{--accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">--}}
                           accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                    <span id="fileName"></span>
                    <br>
                    <input type="submit" name="submit" value="上传">
                </form>
            @endif
            <br>
            <br>
            {{--<a href="{{ route('valueUserReset') }}" style="color: red;">重置价值观用户表</a>--}}
        </div>
    </div>
    <h1>价值观管理</h1>
    <div class="row">

        <div class="col-lg-5 border">

            @if($keywords->count()>4)
                {{--@if(0)--}}
                <a href="{{ route('keywordList') }}">关键词列表</a>

            @else
                <p>关键词文件上传</p>
                <form action="{{ route('keywordListAdd') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <label for="userInput">点击上传</label>
                    <input style="display: none;" type="file" value="" name="userlist" id="userInput"
                           {{--accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">--}}
                           accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                    <span id="fileName"></span>
                    <br>
                    <input type="submit" name="submit" value="上传">
                </form>
            @endif
            <br>
            <br>
            {{--<a href="{{ route('keywordListReset') }}" style="color: red;">重置关键词字典表</a>--}}
        </div>
    </div>
</div>
<script>

    if (document.getElementById('userInput')) {

        document.getElementById('userInput').onchange = function (e) {

            if (e.target.files.length > 0) {
                console.log(e.target.files[0].name)
                document.getElementById('fileName').innerHTML = e.target.files[0].name
            }
        }
    }

</script>
</body>
</html>
