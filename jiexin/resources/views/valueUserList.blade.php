<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>捷信简易管理后台</title>
    <script src="{{asset('/js/app.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">

    <!-- CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->

    <!-- Styles -->
    <style>
        .border {
            border: #5e5d5d solid 1px;
            margin: 5px;
        }

        .padding {
            padding: 5px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>参赛用户列表</h1>
    <div class="row">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>部门</th>
                <th>中文名</th>
                <th>英文名</th>
                <th>角色</th>
                <th>order</th>
                <th>Banner</th>

                {{--{{dd($data)}}--}}
            </tr>
            </thead>
            <tbody>
            @foreach($data as $k=>$v)
                <tr>
                    <th scope="row">{{$k+1}}</th>
                    {{--<td>{{$v['type']}}</td>--}}

                    <td>{{$v['department']}}</td>
                    <td>{{$v['c_name'] . '     '.$v['id']}}</td>
                    <td>{{$v['e_name']}}</td>
                    <td>
                        @if($v['qiye'])
                            企业家 精神
                        @endif

                        @if($v['jieguo'])
                            结果 为导向
                        @endif

                        @if($v['chuangxin'])
                            创新 精神
                        @endif

                        @if($v['gongping'])
                            公平公正
                        @endif
                    </td>
                    <td>{{$v['order']}}</td>
                    <td>
                        <span id="banner-id-{{$v['id']}}">
                            @if($v['banner'])
                                <img height="150px" src="{{$v['banner']}}" alt="">
                            @else
                                <input type="file" class="banner" accept="image/*" name="banner-id-{{$v['id']}}">
                            @endif
                        </span>
                    </td>
                    {{--<th></th>--}}

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.banner').change(function (e) {
        console.log(e)
        let name = e.target.name;
        let file = e.target.files[0];
        let formFile = new FormData();
        formFile.append(name, file);
        $.ajax({
            url: '{{ route('valueBannerAdd') }}?name=' + name,
            type: 'POST',
            data: formFile,
            dataType: "json",
            cache: false,//上传文件无需缓存
            processData: false,//用于对data参数进行序列化处理 这里必须false
            contentType: false, //必须
            success: function (res) {
                if (res.banner) {
                    let str = `<img height="200px" src="${res.banner}" alt="">`
                    $('#' + name).html(str);
                } else {
                    $('#' + name).children()[0].val('')
                }
            },
        })
    })
</script>
</body>
</html>
