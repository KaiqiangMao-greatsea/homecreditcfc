$(function(){
	
	window.lan;
	$('.btn_china').click(function () {
         lan = 'zh_CN';
        window.localStorage.setItem('language', lan);
        lang(lan);
        $('.btn_china').addClass('active');
        $('.btn_english').removeClass('active');
    });
    $('.btn_english').click(function () {
        lan = 'en';
        window.localStorage.setItem('language', lan)
        lang(lan);
        $('.btn_china').removeClass('active');
        $('.btn_english').addClass('active');
    });
    lan = window.localStorage.getItem('language') ? window.localStorage.getItem('language') : 'zh_CN';
    lang(lan);
	
	
	touch.on(".alert_button",'tap',function(){
	 	$(".alert").css("display","none");
	 });
	 
	    window.audio = document.getElementById('switch-music');
        var mp3 = 'mp3/m1.mp3';
        //音乐
        audio = document.createElement('audio'), mSwitch = $('#switch-music'), mTips = $('.tips', mSwitch), isPlay = true, mT = null;
        audio.src = mp3;
        audio.loop = true;
        audio.addEventListener('play', function () {
            mSwitch.addClass('on');
            clearTimeout(mT);

            $("#m_off").css("display", "none");
            mT = setTimeout(function () {
                mTips.removeClass('on')
            }, 1000);
            isPlay = true;
        }, false);
        audio.addEventListener('pause', function () {
            mSwitch.removeClass('on');
            clearTimeout(mT);

            $("#m_off").css("display", "block");
            mT = setTimeout(function () {
                mTips.removeClass('on')
            }, 1000);
            isPlay = false;
        }, false);
        mSwitch.on('touchstart', function () {
            $(this).hasClass('on') ? audio.pause() : audio.play();
            $(this).toggleClass('on')
        });


})
function lang(lan) {
    if (lan == 'zh_CN') {
        $('.btn_english').removeClass('checked-language');
        $('.btn_china').addClass('checked-language');
         $('.btn_china').addClass('active');
        $('.btn_english').removeClass('active');
        
        
    } else {
        $('.btn_china').removeClass('checked-language')
        $('.btn_english').addClass('checked-language')
        
        $('.btn_english').addClass('active');
        $('.btn_china').removeClass('active');
        
    }
    jQuery.i18n.properties({
        name: 'lan',
        mode: 'map',
        cache: true,
        async: true,
        language: lan,
        path: './language/',
        callback: function () {
            $('.login_title').text($.i18n.prop('login.title'));
            $('.login_lang h3').text($.i18n.prop('login.lang'));
            $('label[for="u_id"]').html($.i18n.prop('login.u_id'));
            $('label[for="u_name"]').html($.i18n.prop('login.u_name'));
            $('label[for="u_type"]').html($.i18n.prop('login.IDType'));
            $('#selectType option:eq(0)').html($.i18n.prop('login.IDType_card'));
            $('#selectType option:eq(1)').html($.i18n.prop('login.IDType_passport'));
            $('input[name="u_num"]').attr("placeholder",$.i18n.prop('login.u_num'));
            $('#submit').val($.i18n.prop('login.submit'));
            $('.index_ul li:nth-child(1) img').attr("src",$.i18n.prop('index.list_1'));
            $('.index_ul li:nth-child(2) img').attr("src",$.i18n.prop('index.list_2'));
            $('.index_ul li:nth-child(3) img').attr("src",$.i18n.prop('index.list_3'));
            $('.index_ul li:nth-child(4) img').attr("src",$.i18n.prop('index.list_4'));
            $('.index_ul li:nth-child(5) img').attr("src",$.i18n.prop('index.list_5'));
            $('.index_ul li:nth-child(6) img').attr("src",$.i18n.prop('index.list_6'));
            $(".keyW_circle_one").text($.i18n.prop('keyW.circle_one'))
            $("#menu_li_text1").text($.i18n.prop('keyW.menu_li_one'))
            $("#menu_li_text2").text($.i18n.prop('keyW.menu_li_two'))
            $("#menu_li_text3").text($.i18n.prop('keyW.menu_li_three'))
            $("#menu_li_text4").text($.i18n.prop('keyW.menu_li_four'))
            $(".keyW_right").text($.i18n.prop('keyW.right'))
            $('.rc_main img').attr("src",$.i18n.prop('rc.main'))
            $('.weather_main img').attr("src",$.i18n.prop('weather.main'))
            $('.clothing_main img').attr("src",$.i18n.prop('clothing.main'))
            $('.hotel_main img').attr("src",$.i18n.prop('hotel.main'))
            $(".alert_content h1").text($.i18n.prop('alert.h1'))
            $("#alert1 .alert_msg_one").text($.i18n.prop('alert1.msg_one'))
            $("#alert1 .alert_msg_two").text($.i18n.prop('alert1.msg_two'))
            $("#alert1 .alert_msg_three").text($.i18n.prop('alert1.msg_three'))
            $("#alert1 .alert_msg_four").text($.i18n.prop('alert1.msg_four'))
            $("#alert1 .alert_msg_five").text($.i18n.prop('alert1.msg_five'))
            $("#alert1 .alert_msg_six").text($.i18n.prop('alert1.msg_six'))
            $("#alert4 .alert_msg_one").text($.i18n.prop('alert.msg_one'))
            $("#alert4 .alert_msg_two").text($.i18n.prop('alert.msg_two'))
            $("#alert4 .alert_msg_three").text($.i18n.prop('alert.msg_three'))
            $("#alert4 .alert_msg_four").text($.i18n.prop('alert.msg_four'))
            $("#alert4 .alert_msg_five").text($.i18n.prop('alert.msg_five'))
            $(".alert_button").text($.i18n.prop('alert.button'))
            $(".tip_10").text($.i18n.prop('keyW.tip10'))
            $(".tip_5").text($.i18n.prop('keyW.tip5'))
            $(".tip_11").text($.i18n.prop('keyW.tip11'))
            $(".tip_9").text($.i18n.prop('keyW.tip9'))
            $(".tip_8").text($.i18n.prop('keyW.tip8'))
            $(".tip_6").text($.i18n.prop('keyW.tip6'))
            $(".tip_7").text($.i18n.prop('keyW.tip7'))
            $(".tip_12").text($.i18n.prop('keyW.tip12'))
            $(".tip_18").text($.i18n.prop('keyW.tip18'))
            $(".tip_19").text($.i18n.prop('keyW.tip19'))
            $(".tip_20").text($.i18n.prop('keyW.tip20'))
            $(".tip_16").text($.i18n.prop('keyW.tip16'))
            $(".tip_17").text($.i18n.prop('keyW.tip17'))
            $(".tip_14").text($.i18n.prop('keyW.tip14'))
            $(".tip_15").text($.i18n.prop('keyW.tip15'))
            $(".tip_13").text($.i18n.prop('keyW.tip13'))
            $(".tip_24").text($.i18n.prop('keyW.tip24'))
            $(".tip_27").text($.i18n.prop('keyW.tip27'))
            $(".tip_25").text($.i18n.prop('keyW.tip25'))
            $(".tip_28").text($.i18n.prop('keyW.tip28'))
            $(".tip_26").text($.i18n.prop('keyW.tip26'))
            $(".tip_23").text($.i18n.prop('keyW.tip23'))
            $(".tip_22").text($.i18n.prop('keyW.tip22'))
            $(".tip_21").text($.i18n.prop('keyW.tip21'))
            $(".tip_31").text($.i18n.prop('keyW.tip31'))
            $(".tip_34").text($.i18n.prop('keyW.tip34'))
            $(".tip_36").text($.i18n.prop('keyW.tip36'))
            $(".tip_32").text($.i18n.prop('keyW.tip32'))
            $(".tip_33").text($.i18n.prop('keyW.tip33'))
            $(".tip_35").text($.i18n.prop('keyW.tip35'))
            $(".tip_30").text($.i18n.prop('keyW.tip30'))
            $(".tip_29").text($.i18n.prop('keyW.tip29'))
            $(".cloud_main .title").html($.i18n.prop('cloud.title'))
            $(".cloud_button").text($.i18n.prop('cloud.button'))
            $(".key_word .title").html($.i18n.prop('key_word.title'))      
            $(".menu_li_text1").text($.i18n.prop('keyW.menu_li_one'))
            $(".menu_li_text2").text($.i18n.prop('keyW.menu_li_two'))
            $(".menu_li_text3").text($.i18n.prop('keyW.menu_li_three'))
            $(".menu_li_text4").text($.i18n.prop('keyW.menu_li_four'))
            $("#alert_text1").html($.i18n.prop('alert.text'))
            $("#alert_text2").html($.i18n.prop('alert.text2'))
            $("#bar_msg1").text($.i18n.prop('bar.msg1'))
            $("#bar_msg2").text($.i18n.prop('bar.msg2'))
            $("#bar_msg3").text($.i18n.prop('bar.msg3'))
            $("#bar_msg4").text($.i18n.prop('bar.msg4'))
            $(".rule_msg").text($.i18n.prop('rule.msg'))
            $(".vote_msg").text($.i18n.prop('vote.msg'))
            $(".vote_button").text($.i18n.prop('vote.button'))
        }
    })
}
