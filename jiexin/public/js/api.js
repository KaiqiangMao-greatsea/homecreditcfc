function axiosInit() {
    // http request 拦截器
    axios.interceptors.request.use(
        config => {
            if (getApiToken()) {  // 判断是否存在token，如果存在的话，则每个http header都加上token
                config.headers.Authorization = 'Bearer ' + getApiToken();
            }
            return config;
        },
        err => {
            return Promise.reject(err);
        });

// http response 拦截器
    axios.interceptors.response.use(
        response => {
            return response;
        },
        error => {
            if (error.response) {
                switch (error.response.status) {
                    case 402:
                        setRedirect()
                        window.location.href = '/login.html';
                        break
                    case 401:
                        setRedirect()
                        window.location.href = '/login.html';
                        break
                }
            }
            console.log(error.response)
            return Promise.reject(error.response.data)   // 返回接口返回的错误信息
        });
}

axiosInit();

function request(url, data, type) {
    return axios({
        method: type,
        url: url,
        data: data
    });
}

const API_URL = 'http://jiexin.greatseacn.com/api';
const API_URL_PATH = {

    "path": {
        login: API_URL + '/auth/login',
        valueInfo: API_URL + '/value/user/dep/list',
        valueProgress: API_URL + '/user/value/vote/progress',
        voteValue: API_URL + '/user/value/vote/',
        keywordsProgress: API_URL + '/user/keyword/vote/progress',
        voteKeywords: API_URL + '/user/keyword/vote',
        workConfig: API_URL + '/wechat/work/config',
        valueCount: API_URL + '/user/vote/value/count',
        keywordsCount: API_URL + '/user/keyword/vote/count',
        keywordsAllCount: API_URL + '/user/keyword/all/count',
        valueSwitch: API_URL + '/switch/value/value',
        keywordSwitch: API_URL + '/switch/value/keyword',
    }
}

/**
 *
 * @returns {string | token}
 */
function getApiToken() {
    return localStorage.getItem('API_TOKEN')
}

/**
 *
 * @param token
 * @returns {string|token}
 */
function setApiToken(token) {
    localStorage.setItem('API_TOKEN', token)
    return getApiToken();
}

/**
 *
 * @param data.ID
 * @param data.u_id
 * @param data.u_name
 * @returns {*|PromiseLike<T>|Promise<T>}
 */
function login(data) {
    var url = API_URL_PATH.path.login;
    if (!(data && data.ID && data.u_id)) {
        throw new APIException(404, 'ID Required')
    }
    return request(url, data, "POST").then(function (res) {
        setApiToken(res.data.access_token)
        return Promise.resolve(res);
    })
}

function getRedirect() {
    return localStorage.getItem('redirectUrl')
}

function setRedirect() {
    localStorage.setItem('redirectUrl', window.location.href)
}

function clearRedirect() {
    localStorage.removeItem('redirectUrl')

}

function checkLogin() {
    clearRedirect();
    var apiToken = getApiToken();
    // console.log(apiToken)
    if (!apiToken) {
        setRedirect();
        window.location.href = '/login.html';
    }
}

/**
 *
 * @param code
 * @param msg
 * @constructor
 */
function APIException(code, message) {
    this.msg = message;
    this.code = code;
    this.name = "APIException";
}

function getValueInfo() {
    let url = API_URL_PATH.path.valueInfo
    let data = {};

    return request(url, data, 'GET')
}

function getValueProgress() {
    let url = API_URL_PATH.path.valueProgress
    let data = {};

    return request(url, data, 'GET')
}

function voteValues(type, who) {
    let url = API_URL_PATH.path.voteValue + type + '/' + who
    let data = {};
    return request(url, data, "POST")
}

function getKeywordProgress() {
    let url = API_URL_PATH.path.keywordsProgress
    let data = {};

    return request(url, data, 'GET')
}

function voteKeywords(data) {
    let url = API_URL_PATH.path.voteKeywords

    return request(url, data, "POST")
}

function WorkConfig() {
    document.write('<script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>')
    let url = API_URL_PATH.path.workConfig;
    let data = {url: location.href.split('#')[0]};
    request(url, data, "POST")
        .then(function (res) {
            wx.config(res.data);
            wx.ready(function () {
                wx.hideOptionMenu();
            })
        })
        .catch(function (err) {
            console.log(err)
        })
}

function keywordsCount() {
    return getKeywordProgress()
}

function valueCount() {
    return getValueProgress()
}

function keywordsAllCount() {
    let url = API_URL_PATH.path.keywordsAllCount;
    let data = {};
    return request(url, data, "GET")
}

function getValueStatus() {
    let url = API_URL_PATH.path.valueSwitch;
    let data = {};
    return request(url, data, "GET")
}

function getKeyWordStatus() {
    let url = API_URL_PATH.path.keywordSwitch;
    let data = {};
    return request(url, data, "GET")
}

