var canvas = document.getElementById("loading_canvas");
var stage = new createjs.Stage(canvas);

var manifest;
var preload;
var progressText = new createjs.Text("", "16px Arial", "#000000");
progressText.x = 30-progressText.getMeasuredWidth() / 2;
progressText.y = 50;
stage.addChild(progressText);
stage.update();

//定义相关JSON格式文件列表
function setupManifest() {
    manifest = [
             {src:  "img/icon1.png", id: "icon1"},
	         {src:  "img/icon2.png", id: "icon2"},
	         {src:  "img/icon3.png", id: "icon3"},
	         {src:  "img/icon4.png", id: "icon4"},
	         {src:  "img/icon11.png", id: "icon11"},
	         {src:  "img/icon21.png", id: "icon21"},
	         {src:  "img/icon31.png", id: "icon31"},
	         {src:  "img/icon41.png", id: "icon41"},
	         {src:  "img/icon5.png", id: "icon5"},
	         {src:  "img/icon6.png", id: "icon6"},
	         {src:  "img/icon7.png", id: "icon7"},
	         {src:  "img/word_list1.png", id: "word_list1"},
	         {src:  "img/loading.png", id: "loading"},
	         {src:  "img/loading_1.png", id: "loading_1"},
	         {src:  "img/loading_bottom.png", id: "loading_bottom"}
    ];
}

//开始预加载
function startPreload() {
    preload = new createjs.LoadQueue(true);
    //注意加载音频文件需要调用如下代码行
    preload.installPlugin(createjs.Sound);         
    preload.on("fileload", handleFileLoad);
    preload.on("progress", handleFileProgress);
    preload.on("complete", loadComplete);
    preload.on("error", loadError);
    preload.loadManifest(manifest);
 
}

//处理单个文件加载
function handleFileLoad(event) {
    console.log("文件类型: " + event.item.type);
    if(event.item.id == "logo"){
        console.log("logo图片已成功加载");
    }
}
 
//处理加载错误：大家可以修改成错误的文件地址，可在控制台看到此方法调用
function loadError(evt) {
    console.log("加载出错！",evt.text);
}
 
//已加载完毕进度 
function handleFileProgress(event) {
     progressText.text = (preload.progress*100|0) + "%";
    $("#progress-bar").css("width",progressText.text );
   
    stage.update();
    
    
}

//全度资源加载完毕
function loadComplete(event) {
  console.log("已加载完毕全部资源");  
	$("#loading").css("display","none");
	audio.play();
	
}

setupManifest();
startPreload();