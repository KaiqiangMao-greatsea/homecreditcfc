<?php

namespace App\Http\Controllers;

//use App\Models\KeyWords;
use App\Models\KeyWordsDic;
use App\Models\ValueUsers;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $switcher = new \App\Http\Controllers\SwitchController();
        $user = new User();
        $valueUser = new ValueUsers();
        $keywords = new KeyWordsDic();
        return view('admin', [
            'switcher' => $switcher,
            'user' => $user,
            'valueUser' => $valueUser,
            'keywords'=>$keywords
        ]);
    }
}
