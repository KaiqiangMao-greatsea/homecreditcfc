<?php

namespace App\Http\Controllers;

use App\Models\ValueUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use \PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class ValueUserController extends Controller
{
    private $sheetname = '2017 Value Stars';
    private $spreadsheet = '';
    private $user = [];
    private $importLock = 'valueImportLock.lock';

    public function command(Request $request)
    {
//        $path = '/home/vagrant/code/greatsea/homecreditcfc/jiexin/storage/app/valUserlist/hNRJluNr79bXOtM90f6t6d3gqmTlFXiFecqBUgq8.xlsx';
//        $this->foreachUserFile($path);
    }

    public function usersReset(Request $request)
    {
        if (Storage::exists($this->importLock)) {
            if ($request->get('vauleUserReset') == 'enter') {
                Storage::delete($this->importLock);
                DB::table('value_users')->truncate();
                return redirect()->route('adminIndex');
            }
            dd('该操作风险大，会清空价值观选手信息，请在url中增加" vauleUserReset=enter "执行表重置');
        } else {
            $count = ValueUsers::count();
            dd($count . '未发现锁定文件');
        }
    }

    public function userList(Request $request)
    {

        $data = ValueUsers::UserList();
        return view('valueUserList', ['data' => $data]);

    }

    public function userByDeP()
    {

        $res = ValueUsers::userListByDep();
        return response()->json($res);

    }

    public function userBanner(Request $request)
    {
        $name = $request->get('name');
        $id = explode('-', $name)[2];
        if ($request->has($name)) {
            $path = '/storage/' . Storage::disk('public')->putFile('banner', $request->file($name));
            ValueUsers::where('id', $id)->update(['banner' => $path]);
        }
        return ValueUsers::find($id)->toArray();
    }

    public function userImport(Request $req)
    {
        if (Storage::exists($this->importLock)) {
            dd('已经上传过了，先删除锁定文件');
        }
        if ($req->hasFile('userlist')) {

            $path = storage_path() . '/app/' . $req->file('userlist')->store('valUserlist');
            var_dump($path);
            return $this->foreachUserFile($path);
        } else {
            dd('请上传文件');
        }
    }

    private function foreachUserFile($path)
    {
        $reader = new Xlsx();
        $reader->setLoadSheetsOnly($this->sheetname);
        $reader->setReadDataOnly(true);
        $this->spreadsheet = $spreadsheet = $reader->load($path)->getActiveSheet();

        $highestRow = $spreadsheet->getHighestRow(); // e.g. 10
        $highestColumn = $spreadsheet->getHighestColumn(); // e.g 'F'

        if ($highestColumn != "L") {
            dd('表头总数不为L个，请按约定表格上传');
        }
        $role = '';
        for ($row = 3; $row <= $highestRow; $row++) {

            switch ($this->sqlStrFormat('A', $row)) {
                case boolval(stristr($this->sqlStrFormat('A', $row), 'entrepreneurial')):
                    $role = 'qiye';
                    ++$row;
                    break;
                case boolval(stristr($this->sqlStrFormat('A', $row), 'results')):
                    $role = 'jieguo';
                    ++$row;

                    break;
                case boolval(stristr($this->sqlStrFormat('A', $row), 'Innovative')):
                    $role = 'chuangxin';
                    ++$row;

                    break;
                case boolval(stristr($this->sqlStrFormat('A', $row), 'Fair')):
                    $role = 'gongping';
                    ++$row;

                    break;
                default:

                    break;
            }
            $userTmp = [];
            if ($role) {
                $userTmp['department'] = $this->trimChinese('C', $row);
                $userTmp['c_name'] = $this->sqlStrFormat('E', $row);
                $userTmp['e_name'] = $this->sqlStrFormat('F', $row);
                $userTmp['title'] = $this->sqlStrFormat('H', $row);
                $userTmp['phone'] = $this->sqlStrFormat('I', $row);
                $userTmp[$role] = 1;
            }
            array_push($this->user, $userTmp);
        }
//        var_dump($this->user);
        return $this->insertUser();
    }

    private function insertUser()
    {
        foreach ($this->user as $v) {
            $user = new ValueUsers();
            foreach ($v as $k => $vv) {
                $user->$k = $vv;
            }
            $user->save();
        }

//        $step = 20;
//        for ($i = 0; $i < count($this->user); $i += $step) {
//            $tmp = array_slice($this->user, $i, $step);
//            DB::table('value_users')->insert($tmp);
////            var_dump('插入数据');
////            var_dump($tmp);
//        }
        Storage::put($this->importLock, time());
        Cache::forget('userListByDep:all');
        return redirect()->route('valueUserList');
    }


    private function sqlStrFormat($AZcolumn, $row)
    {
        $ColumnIndex = Coordinate::columnIndexFromString($AZcolumn);

        return strval(preg_replace('/ /', '', strtolower(trim($this->spreadsheet->getCellByColumnAndRow($ColumnIndex, $row)->getValue()))));
    }

    private function trimChinese($col, $row)
    {
        $str = $this->sqlStrFormat($col, $row);
        $str = str_replace('，', ',', $str);
        $str = preg_replace('/([\x80-\xff]*)/i', '', $str);
        $str = preg_replace('/\n/i', '', $str);
        return strval($str);
    }


}
