<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use \PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    private $sheetname = 'Namelist';
    private $spreadsheet = '';
    private $user = [];
    private $importLock = 'importLock.lock';

    public function command(Request $request)
    {
//        $path = '/home/vagrant/code/greatsea/homecreditcfc/jiexin/storage/app/userlis/lVYjAK0mROzftSex4y6qRsmhoYR3ba3QGtjnvCJn.xlsx';
//        $this->foreachUserFile($path);
    }

    public function usersReset(Request $request)
    {
        if (Storage::exists($this->importLock)) {
            if ($request->get('userreset') == 'enter') {
                Storage::delete($this->importLock);
                DB::table('users')->truncate();
                return redirect()->route('adminIndex');
            }
            dd('该操作风险大，会清空所有参与投票用户和价值观选手信息，请在url中增加" userreset=enter "执行用户表重置');
        } else {
            $count = User::count();
            dd($count . '未发现锁定文件，');
        }
    }

    public function userList(Request $request)
    {
        $data = User::UserList();
        return view('userList', ['data' => $data]);
    }

    public function userImport(Request $req)
    {
        if (Storage::exists($this->importLock)) {
            dd('已经上传过了，先删除锁定文件');
        }
        if ($req->hasFile('userlist')) {

            $path = storage_path() . '/app/' . $req->file('userlist')->store('userlist');
            var_dump($path);
            return $this->foreachUserFile($path);
        } else {
            dd('请上传文件');
        }
    }

    private function foreachUserFile($path)
    {
        $reader = new Xlsx();
        $reader->setLoadSheetsOnly($this->sheetname);
        $reader->setReadDataOnly(true);
        $this->spreadsheet = $spreadsheet = $reader->load($path)->getActiveSheet();

        $highestRow = $spreadsheet->getHighestRow(); // e.g. 10
        $highestColumn = $spreadsheet->getHighestColumn(); // e.g 'F'

        if ($highestColumn != "O") {
            dd('表头总数不为字母O 个，请按约定表格上传');
        }
        for ($row = 2; $row <= $highestRow; $row++) {

            $userTmp = [];
            $userTmp['type'] = $this->sqlStrFormat('B', $row);
            $userTmp['department'] = $this->sqlStrFormat('C', $row);
            $userTmp['e_id'] = $this->sqlStrFormat('D', $row);
            $userTmp['c_name'] = $this->sqlStrFormat('E', $row);
            $userTmp['e_name'] = $this->sqlStrFormat('F', $row);
            $userTmp['title'] = $this->sqlStrFormat('G', $row);
            $userTmp['phone'] = $this->sqlStrFormat('I', $row);
            $userTmp['gender'] = $this->sqlStrFormat('J', $row) == 'male' ? 1 : 0;
            $userTmp['email'] = $this->sqlStrFormat('K', $row);
            $userTmp['id_type'] = stristr($this->sqlStrFormat('L', $row), 'passport') === false ? 1 : 0;
            $userTmp['id_number'] = $this->sqlStrFormat('M', $row);
            array_push($this->user, $userTmp);
//            var_dump($userTmp);
//            var_dump($this->sqlStrFormat('A', $row));
//            ob_flush();
        }
        return $this->insertUser();
    }

    private function insertUser()
    {
        $step = 20;
        for ($i = 0; $i < count($this->user); $i += $step) {
            $tmp = array_slice($this->user, $i, $step);
            DB::table('users')->insert($tmp);
//            var_dump('插入数据');
//            var_dump($tmp);
        }
        Storage::put($this->importLock, time());
        return redirect()->route('userList');
    }


    private function sqlStrFormat($AZcolumn, $row)
    {
        $ColumnIndex = Coordinate::columnIndexFromString($AZcolumn);
        return strval(preg_replace('/ /', '', strtolower(trim($this->spreadsheet->getCellByColumnAndRow($ColumnIndex, $row)->getValue()))));

//        return strval(strtolower(trim($this->spreadsheet->getCellByColumnAndRow($ColumnIndex, $row)->getValue())));
    }
}
