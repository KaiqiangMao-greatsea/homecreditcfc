<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use EasyWeChat\Factory;

class WorkController extends Controller
{
    public $work = '';

    public function __construct()
    {
        $config = config('wechat.work.default');
        $this->work = Factory::work($config);
    }

    public function Config(Request $req)
    {
        $url = $req->get('url');
        $APIs = ['hideOptionMenu','hideMenuItems'];
        $this->work->jssdk->setUrl($url);
        $data = $this->work->jssdk->buildConfig($APIs, $debug = false, $beta = false, $json = false);
        return response()->json($data);
    }
}
