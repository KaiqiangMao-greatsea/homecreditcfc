<?php

namespace App\Http\Controllers;

use App\Models\Keywords;
use App\Models\KeyWordsRanks;
use App\Models\Values;
use App\Models\ValuesRanks;
use App\Models\ValueUsers;
use App\User;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class DowloadExcelController extends Controller
{
    public $writer = '';
    public $fileName = '';
    private $spreadsheet = '';

    public function __construct()
    {
        $this->spreadsheet = new Spreadsheet();
    }

    public function ValueRanks()
    {
        $row = 1;
        $data = [
            $row => [
                'A' => '员工ID',
                'B' => '员工部门',
                'C' => '员工姓名',
                'D' => '投票结束名次',
            ]
        ];
        $res = ValuesRanks::getRanksInfo();
        foreach ($res as $v) {
            ++$row;
            $data[$row] = [
                'A' => $v['e_id'],
                'B' => $v['department'],
                'C' => $v['c_name'],
                'D' => $v['order'],
            ];
        }
        return $this->NewXlsX('价值观之星投票排名', $data);
    }

    public function ValuesCountAll()
    {
        $row = 1;
        $data = [
            $row => [
                'A' => '价值观',
                'B' => '候选人ID',
                'C' => '候选人部门',
                'D' => '候选人姓名',
                'E' => '所获得的总票数',
            ]
        ];
        $valueusers = ValueUsers::userListByDep(false);
        foreach ($valueusers as $k => $v) {
            if ($k == 'qiye') {
                $jingshen = '我践行企业家精神';
                foreach ($v as $vv) {
                    ++$row;
                    $data[$row] = [
                        'A' => $jingshen,
                        'B' => $vv['id'],
                        'C' => $vv['department'],
                        'D' => $vv['c_name'],
                        'E' => $vv['count'],
                    ];
                }
            }
            if ($k == 'jieguo') {
                $jingshen = '我践行以结果为导向';
                foreach ($v as $vv) {
                    ++$row;
                    $data[$row] = [
                        'A' => $jingshen,
                        'B' => $vv['id'],
                        'C' => $vv['department'],
                        'D' => $vv['c_name'],
                        'E' => $vv['count'],
                    ];
                }
            }
            if ($k == 'chuangxin') {
                $jingshen = '我践行创新精神';
                foreach ($v as $vv) {
                    ++$row;
                    $data[$row] = [
                        'A' => $jingshen,
                        'B' => $vv['id'],
                        'C' => $vv['department'],
                        'D' => $vv['c_name'],
                        'E' => $vv['count'],
                    ];
                }
            }
            if ($k == 'gongping') {
                $jingshen = '我践行公平公正';
                foreach ($v as $vv) {
                    ++$row;
                    $data[$row] = [
                        'A' => $jingshen,
                        'B' => $vv['id'],
                        'C' => $vv['department'],
                        'D' => $vv['c_name'],
                        'E' => $vv['count'],
                    ];
                }
            }
        }
        return $this->NewXlsX('候选人获得的总票数', $data);
    }

    private function map($AZ = 'D', $max = 15)
    {
        for ($i = 1; $i <= $max; $i++) {
            if ($max == 15 && $i == 3) {
                ++$i;
            }
            yield $i => $AZ;
            $AZ++;
        }
    }

    public function UserValueVoteInfo()
    {
        $row = 1;
        $data = [
            $row => [
                'A' => '员工ID',
                'B' => '员工部门',
                'C' => '员工姓名',
            ]
        ];
        $map = iterator_to_array($this->map());
        $res = ValueUsers::all();
        $col = 'D';
        foreach ($res as $v) {
            $data[$row][$col] = $v['id'] . '-' . $v['c_name'];
            ++$col;
        }
//        $res = Values::leftJoin('users', 'users.id', 'values.uid')->get()->toArray();
        $users = User::get()->toArray();
        foreach ($users as $v) {
            ++$row;

            $tmp = Values::where('uid', $v['id'])->get()->toArray();
            $data[$row] = [
                'A' => $v['e_id'],
                'B' => $v['department'],
                'C' => $v['c_name'],
            ];

            foreach ($tmp as $vv) {
                if ($vv['qiye']) {
                    $data[$row][$map[$vv['qiye']]] = $vv['qiye'] > 0 ? 1 : 0;
                }
                if ($vv['jieguo']) {
                    $data[$row][$map[$vv['jieguo']]] = $vv['jieguo'] > 0 ? 1 : 0;
                }
                if ($vv['chuangxin']) {
                    $data[$row][$map[$vv['chuangxin']]] = $vv['chuangxin'] > 0 ? 1 : 0;
                }
                if ($vv['gongping']) {
                    $data[$row][$map[$vv['gongping']]] = $vv['gongping'] > 0 ? 1 : 0;
                }

            }
        }
        return $this->NewXlsX('用户投票详情', $data);
    }

    public function KeyWordRanks()
    {
        $row = 1;
        $data = [
            $row => [
                'A' => '员工ID',
                'B' => '员工部门',
                'C' => '员工姓名',
                'D' => '投票结束名次',
            ]
        ];
        $res = KeyWordsRanks::getRanksInfo();
        foreach ($res as $v) {
            ++$row;
            $data[$row] = [
                'A' => $v['e_id'],
                'B' => $v['department'],
                'C' => $v['c_name'],
                'D' => $v['order'],
            ];
        }
        return $this->NewXlsX('价值观词云投票排名', $data);
    }

    public function keywordsInfo()
    {
        $tem = storage_path() . '/app/keywordsInfo.xlsx';
        $this->spreadsheet = IOFactory::load($tem);

        $res = Keywords::valueAllCount(false);
        foreach ($res as $v) {
            $data[$v['id'] - 3] = [
                'C' => $v['count']
            ];
        }
//        dd($res, $data);

        return $this->NewXlsX('价值观词云的总票数', $data);
    }

    public function valuesInfo()
    {
        $tem = storage_path() . '/app/valuesInfo.xlsx';
        $this->spreadsheet = IOFactory::load($tem);

//        $user = User::leftjoin('keywords', 'keywords.uid', 'users.id')->get()->toArray();
        $user = User::all()->toArray();
        $map = iterator_to_array($this->map('D', 32));
        $row = 3;
        foreach ($user as $k => $v) {
            $data[$row] = [
                'A' => $v['e_id'],
                'B' => $v['department'],
                'C' => $v['c_name'],
            ];
            $res = Keywords::where('uid', $v['id'])->get()->toArray();
            foreach ($res as $vv) {
                $col = $vv['kid'] - 4;
                $data[$row][$map[$col]] = 1;
            }
//            dd($data, $res);
            ++$row;
        }
        return $this->NewXlsX('用户选择的词云数据', $data);
    }

    private
    function NewXlsX($fileName, $data)
    {
        $this->writer = new Xlsx($this->spreadsheet);

        $sheet = $this->spreadsheet->getActiveSheet();

        foreach ($data as $k => $v) {
            foreach ($v as $kk => $vv) {
                if ($k < 3 && isset($vv[''])) {

                }
                $sheet->setCellValue(strval($kk) . strval($k), $vv);
            }
        }

        $path = storage_path() . '/app/';
        $fileName .= '.xlsx';
        $file = $path . $fileName;
        $this->writer->save($file);


        return response()->download($file, $fileName)->deleteFileAfterSend(true);

    }


}
