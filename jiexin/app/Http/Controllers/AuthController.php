<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\User;

class AuthController extends Controller
{
    private $exp = 7200 * 6 * 12; //30天

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $this->validate($request, [
            'u_id' => 'required',
            'ID' => 'required',
//            'id_type' => 'required'
        ]);

        $credentials = request(['u_id', 'ID', 'id_type']);

        $user = User::checkUser($credentials['u_id'], $credentials['ID'], $credentials['id_type'] ?? 1);


        if (!$user) {
            return response()->json(['code' => 404, 'msg' => 'user not found'], 455);
        }

        $token = '';
        if (strtolower($credentials['u_id']) == strtolower($user['e_id'])) {
            $token = auth()->setTTL($this->exp)->tokenById($user['id']);
        }
        if (!$token) {
            return response()->json(['code' => 500, 'msg' => 'retry'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}