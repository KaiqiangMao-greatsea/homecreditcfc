<?php

namespace App\Http\Controllers;

use App\Models\KeyWordsDic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use \PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use \PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use \PhpOffice\PhpSpreadsheet\Writer\IWriter;

class KeywordController extends Controller
{
    private $sheetname = 'words version';
    private $spreadsheet = '';
    private $KW = [];
    private $importLock = 'KeywordsImportLock.lock';

    public function KeyWordsListByCate()
    {
        $data = KeyWordsDic::KeyWordsListByCate();
        dd($data);
    }

    public function AkeywordListAdd()
    {
        $path = '/home/vagrant/code/greatsea/homecreditcfc/jiexin/storage/app/keyWordList/1jylr3EBG3Ehnb11Ec7M6yfya3Z2yc3HMnleT00T.xlsx';
        return $this->foreachKWFile($path);
    }

    public function keywordListAdd(Request $req)
    {
        if (Storage::exists($this->importLock)) {
            dd('已经上传过了，先删除锁定文件');
        }
        if ($req->hasFile('userlist')) {

            $path = storage_path() . '/app/' . $req->file('userlist')->store('keyWordList');
            var_dump($path);
            return $this->foreachKWFile($path);
        } else {
            dd('请上传文件');
        }
    }

    private function foreachKWFile($path)
    {
        $reader = new Xlsx();
        $reader->setLoadSheetsOnly($this->sheetname);
        $reader->setReadDataOnly(true);
        $this->spreadsheet = $spreadsheet = $reader->load($path)->getActiveSheet();

        $highestRow = $spreadsheet->getHighestRow(); // e.g. 10
        $highestColumn = $spreadsheet->getHighestColumn(); // e.g 'F'

        if ($highestColumn != "D") {
            dd('表头总数不为D个，请按约定表格上传');
        }

        for ($col = 'A'; $col <= $highestColumn; $col++) {
            $switch = $this->sqlStrFormat(1, $col);
            switch ($switch) {
                case boolval(stristr($switch, 'entrepreneurial')):
                    $pid = 1;
                    break;
                case boolval(stristr($switch, 'results')):
                    $pid = 2;
                    break;
                case boolval(stristr($switch, 'Innovative')):
                    $pid = 3;
                    break;
                case boolval(stristr($switch, 'Fair')):
                    $pid = 4;
                    break;
                default:
                    break;
            }
            if ($pid) {
                for ($row = 2; $row <= $highestRow; $row++) {
                    $str = $this->sqlStrFormat($row, $col);
                    if (boolval($str)) {
                        $cstr = $this->getTxt($str, true);
                        $estr = $this->getTxt($str, false);;
                        $tmp['c_name'] = $cstr;
                        $tmp['e_name'] = $estr;
                        $tmp['pid'] = $pid;
//                        var_dump($str);
                        array_push($this->KW, $tmp);
                    }
                }

            }
        }
//        var_dump($this->user);
        return $this->insertUser();
    }

    private function getTxt($str, $CT = true)
    {
        preg_match("/[\x{4e00}-\x{9fa5}]+/u", $str, $c);
        $e = str_replace($c[0], '', $str);
//        dd($c, $e);
        if ($CT) {
            return $c[0];
        }
        return $e;
    }

    private function insertUser()
    {
//        dd($this->KW);
        foreach ($this->KW as $v) {
            $user = new KeyWordsDic();
            foreach ($v as $k => $vv) {
                $user->$k = $vv;
            }
            $user->save();
        }

        Storage::put($this->importLock, time());
        Cache::forget('KWListByCate:all');
        return redirect()->route('keywordList');
    }

    private function sqlStrFormat($row, $AZcolumn)
    {
        $ColumnIndex = Coordinate::columnIndexFromString($AZcolumn);
        $str = $this->spreadsheet->getCellByColumnAndRow($ColumnIndex, $row)->getValue();
//        var_dump($AZcolumn . $row . ':' . $str);
        return strval(trim($str));
    }

    public function keywordList()
    {
        $data = KeyWordsDic::KeyWordsListByCate();
//        dd($data);
        return view('keywords', ['data' => $data]);
    }

    public function keywordListByCate()
    {
        $data = KeyWordsDic::KeyWordsListByCate();
//        dd($data);
        return response()->json($data);
//        return view('keywords', ['data' => $data]);
    }
    public function keywordListReset(Request $request)
    {
        if (Storage::exists($this->importLock)) {

            if ($request->get('keywordsUserReset') == 'enter') {
                Storage::delete($this->importLock);
                DB::table('value_users')->truncate();
                return redirect()->route('adminIndex');
            }
            dd('该操作风险大，会清空价值观信息，请在url中增加" keywordsUserReset=enter "执行表重置');
        } else {
            $count = KeyWordsDic::count();
            dd($count . '未发现锁定文件');
        }
    }
}
