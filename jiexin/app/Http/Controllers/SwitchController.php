<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Switchs;

class SwitchController extends Controller
{
    public function command(Request $request)
    {
        $data = $request->all();

        if ($request->get('type') == 'value') {
            switch ($data['action'] ?? '') {
                case 1:
                    $this->valueStart();
                    break;
                case 2:
                    $this->valueEnd();
                    break;
                case 0:
                    $this->valueOff();
                    break;
                default:
                    $this->valueStatus();
                    break;
            }
            return;

        }

        if ($request->get('type') == 'keyword') {
            switch ($data['action'] ?? '') {
                case 1:
                    $this->keywordStart();
                    break;
                case 2:
                    $this->keywordEnd();
                    break;
                case 0:
                    $this->keywordOff();
                    break;
                default:
                    $this->keywordStatus();
                    break;
            }
            return;
        }
        $this->keywordStatus();
        $this->valueStatus();
    }

    public function valueStart()
    {
        Switchs::changeStatus('value', 1);
        $this->valueStatus();
    }

    public function valueEnd()
    {
        Switchs::changeStatus('value', 2);
        $this->valueStatus();
    }

    public function valueOff()
    {
        Switchs::changeStatus('value', 0);
        $this->valueStatus();
    }

    public function valueStatus()
    {
        $res = Switchs::getStatus('value');
        print_r('value 价值观投票 当前状态：' . $res . ' ;<br>参考值 0 关闭；1开启; 2结束；<br>' . PHP_EOL);
    }

//关键词活动
    public function keywordStart()
    {
        Switchs::changeStatus('keyword', 1);
        $this->keywordStatus();
    }

    public function keywordEnd()
    {
        Switchs::changeStatus('keyword', 2);
        $this->keywordStatus();
    }

    public function keywordOff()
    {
        Switchs::changeStatus('keyword', 0);
        $this->keywordStatus();
    }

    public function keywordStatus()
    {
        $res = Switchs::getStatus('keyword');
        print_r('keyword 关键词投票 当前状态：' . $res . ' ;<br>参考值 0 关闭；1开启; 2结束；<br>' . PHP_EOL);
    }

    public function getSwitchAPI($type)
    {
        if ($type == 'keyword') {

            $res = Switchs::getStatus('keyword');

        }

        if ($type == 'value') {

            $res = Switchs::getStatus('value');


        }
        $date = ['type' => $type, 'status' => $res];
        if ($res == 0) {
            $date['msg'] = $type . '关闭';
        } elseif ($res == 1) {
            $date['msg'] = $type . '开启';
        } elseif ($res == 2) {
            $date['msg'] = $type . '结束';
        }

        return response()->json($date);
    }
}
