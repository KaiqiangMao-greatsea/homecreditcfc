<?php

namespace App\Http\Controllers;

use App\Models\Keywords;
use App\Models\KeyWordsDic;
use App\Models\KeyWordsRanks;
use App\Models\Switchs;
use App\Models\Values;
use App\Models\ValuesRanks;
use App\Models\ValueUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VoteContoller extends Controller
{
    private static function getMe()
    {
        $me = auth()->user()->toArray();
        return $me;
    }

    public function voteValue($type, $who)
    {
        if (in_array($type, ["qiye", "jieguo", "chuangxin", "gongping"])) {
            //判断活动开关
            $sw = Switchs::getStatus('value');
            if ($sw == 1) {

                $me = self::getMe();
                //判断 $who 在不在 $type  k=>
                $inType = ValueUsers::inType($type, $who);
                if ($inType) {

                    //先判断同一价值观投了几票

                    $arrVoteInfo = Values::voteInfo($type, $me['id']);
                    try {
                        DB::beginTransaction();

                        switch (count($arrVoteInfo)) {
                            case 0;
                                $res = Values::vote($me, $type, $who);
                                break;
                            case 1:
                                $first = current($arrVoteInfo);
//                                dd($arrVoteInfo, $first, $who);
                                //判断是不是重复提交
                                if ($who == $first[$type]) {
                                    $data = ['code' => 403, 'msg' => '重复提交，已经投过他的票了', 'data' => ''];
                                    return response()->json($data, 455);
                                }
                                $depSame = ValueUsers::isSameDep($me, $who);
                                if ($depSame) {
                                    //判断first是不是同一部门
                                    $depSame = ValueUsers::isSameDep($me, $first[$type]);
                                    if ($depSame) {
                                        $data = ['code' => 405, 'msg' => '同一价值观只可以投本部门一票', 'data' => ''];
                                        return response()->json($data, 455);
                                    }
                                }
                                $res = Values::vote($me, $type, $who);

                                break;
                            case 2:
                                $data = ['code' => 406, 'msg' => '同一价值观只可以投2票', 'data' => ''];
                                return response()->json($data, 455);
                                break;
                            default:
                                return;
                                break;
                        }
                        //如果票数大于1 判断是不是同部门
                        //如果是，判断当前是不是同部门
                        //如果不是 进行投票
                        $num = count(Values::voteInfo($type, $me['id']));
//                    dd(111,$num);
                        $data = ['code' => 200, 'msg' => '投票成功', 'data' => ['type' => $type, 'num' => $num]];
                        if ($num == 2 && $type == 'gongping') {
                            $end = ValuesRanks::addRanks($me['id']);
                            $data['data']['ranks'] = $end;
                        }
                        DB::commit();
                        return response()->json($data);
                        //返回当前用户在在前价值观投票的个数
                    } catch (\Exception $e) {
                        DB::rollBack();
                        $data = ['code' => 500, 'msg' => '重新尝试', 'data' => $e];
                        return response()->json($data, 500);
                    }
                }
                Log::error($who . '不在类型：' . $type);
                return;
            }
            if ($sw == 2) {
                $data = ['code' => 302, 'msg' => '活动已结束', 'data' => ''];
                return response()->json($data, 455);
            }
            $data = ['code' => 300, 'msg' => '活动未开启', 'data' => ''];
            return response()->json($data, 455);
        } else {
            $data = ['code' => 404, 'msg' => 'type not found', 'data' => ''];
            return response()->json($data, 455);
        }

    }

    public function valueProgress()
    {
        $userId = auth()->user()->toArray();
        $data = Values::getVoteById($userId['id']);
        return response()->json($data);
    }

    public function voteKeyword(Request $request)
    {
        $sw = Switchs::getStatus('keyword');
        $data = $request->get('data');
        if ($sw == 1) {
            $me = self::getMe();
            $count = Keywords::where('uid', $me['id'])->count();
            if ($count > 0) {
                $data = ['code' => 405, 'msg' => '已经投过票了', 'data' => ''];
                return response()->json($data, 455);
            }
            try {
                DB::beginTransaction();
                foreach ($data as $v) {
                    Keywords::add($v, $me['id']);
                }
                $end = KeyWordsRanks::addRanks($me['id']);
                DB::commit();
                $data = ['code' => 200, 'msg' => '投票成功', 'data' => ['ranks' => $end]];
                return response()->json($data);
            } catch (\Exception $e) {
                DB::rollBack();
                $data = ['code' => 500, 'msg' => '重新尝试', 'data' => $e];
                return response()->json($data, 500);

            }
        }
        if ($sw == 2) {
            $data = ['code' => 302, 'msg' => '活动已结束', 'data' => ''];
            return response()->json($data, 455);
        }
        $data = ['code' => 300, 'msg' => '活动未开启', 'data' => ''];
        return response()->json($data, 455);
    }

    public function voteKeywordBAK($type, $kid)
    {
        if (in_array($type, ["qiye", "jieguo", "chuangxin", "gongping"])) {
            //判断活动开关
            $sw = Switchs::getStatus('keyword');
            if ($sw == 1) {

                $me = self::getMe();
                //判断 $who 在不在 $type  k=>
                $inType = KeyWordsDic::inType($type, $kid);
                if ($inType) {

                    //先判断同一价值观投了几票

                    $arrVoteInfo = Keywords::voteInfo($type, $me['id']);
                    try {
                        DB::beginTransaction();
                        $count = count($arrVoteInfo);
                        if ($count == 0) {
                            $res = Keywords::vote($me, $kid);
                        } elseif ($count < 3) {
                            //判断是不是重复提交
                            foreach ($arrVoteInfo as $v) {
                                if ($v['kid'] == $kid) {
                                    $data = ['code' => 403, 'msg' => '重复提交，已经投过本关键词', 'data' => ''];
                                    return response()->json($data, 404);
                                }
                            }
                            $res = Keywords::vote($me, $kid);
                        } elseif ($count = 3) {
                            $data = ['code' => 405, 'msg' => '最多只能投3票', 'data' => ''];
                            return response()->json($data, 404);
                        } else {

                        }

                        $num = count(Keywords::voteInfo($type, $me['id']));
                        $data = ['code' => 200, 'msg' => '投票成功', 'data' => ['type' => $type, 'num' => $num]];
                        if ($num == 3 && $type == 'gongping') {
                            $end = ValuesRanks::addRanks($me['id']);
                            $data['data']['ranks'] = $end;
                        }
                        DB::commit();

                        //返回当前用户在在前价值观投票的个数
                        return response()->json($data);
                    } catch (\Exception $e) {
                        DB::rollBack();
                        $data = ['code' => 500, 'msg' => '重新尝试', 'data' => $e];
                        return response()->json($data, 500);
                    }

                }
                Log::error($kid . '不在类型：' . $type);
                return;
            }
            if ($sw == 2) {
                $data = ['code' => 302, 'msg' => '活动已结束', 'data' => ''];
                return response()->json($data, 404);
            }
            $data = ['code' => 300, 'msg' => '活动未开启', 'data' => ''];
            return response()->json($data, 404);
        } else {
            $data = ['code' => 404, 'msg' => 'type not found', 'data' => ''];
            return response()->json($data, 404);
        }
    }

    public function keywordProgress()
    {
        $userId = auth()->user()->toArray();
        $data = Keywords::getVoteById($userId['id']);
        return response()->json($data);
    }

    public function valueCount()
    {
        $userId = auth()->user()->toArray();
        $data = Values::getVoteById($userId['id']);
        return response()->json($data);
    }

    public function keywordCount()
    {
        $userId = auth()->user()->toArray();
        $data = Keywords::getVoteById($userId['id']);
        return response()->json($data);
    }

    public function valueAllCount()
    {
//        $res = Keywords::select('count(*)')->where(DB::raw())->groupBy('kid')->get()->toArray();
        $res = Keywords::valueAllCount();
        return response()->json($res);
    }
}
