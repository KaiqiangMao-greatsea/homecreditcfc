<?php

namespace App;

use App\Models\Values;
use Illuminate\Support\Facades\Cache;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

//use Value

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'department', 'e_id', 'c_name', 'e_name', 'id_type', 'id_number', 'title', 'email', 'phone', 'gender', 'status'
    ];


    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function checkUser($uid, $ID, $id_type = 1)
    {
        $user = User::where('e_id', strtolower($uid))->first();
        if (!$user) {
            return false;
        }
        $user = $user->toArray();
        if ($id_type) {
            if ($ID == substr($user['id_number'], -6, 6)) {
                return $user;
            }
        } else {
            if ($ID == $user['id_number']) {
                return $user;
            }
        }
        return false;
    }

    /**
     * @param string $filter
     * @return array|mixed
     */
    public static function UserList()
    {
        $min = 15;

        $data = Cache::get('userList:all');
        if (is_null($data)) {
            $data = self::all()->toArray();
            Cache::put('userList:all', json_encode($data), $min);
            return $data;
        }
        return json_decode($data, true);
    }

    public static function ValuesInfo()
    {
        $data = self::UserList();
        $user = [
            "qiye" => [],
            "jieguo" => [],
            "chuangxin" => [],
            "gongping" => []
        ];
        foreach ($data as $v) {
            if ($v['qiye']) {
                $v['count'] = Values::where('qiye', $v['id'])->count();
                $user['qiye'][$v['id']] = $v;
            }

            if ($v['jieguo']) {
                $v['count'] = Values::where('qiye', $v['id'])->count();
                $user['jieguo'][$v['id']] = $v;
            }

            if ($v['chuangxin']) {
                $v['count'] = Values::where('qiye', $v['id'])->count();
                $user['chuangxin'][$v['id']] = $v;
            }

            if ($v['gongping']) {
                $v['count'] = Values::where('gongping', $v['id'])->count();
                $user['gongping'][$v['id']] = $v;
            }

        }
        return $user;
    }

    public static function sameDepartment()
    {

    }
}
