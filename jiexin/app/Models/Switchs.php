<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Switchs extends Model
{
    static private $switch = ['value', 'keyword'];
    protected $table = 'switchs';

    static public function getStatus($type)
    {
        if (in_array($type, self::$switch)) {

            $status = Cache::get('switch:' . $type);
//            var_dump($status);
            if (is_null($status)) {
                $me = new self();
                $res = $me->where('activity', $type)->first()->toArray();
                if (isset($res['status'])) {
                    Cache::forever('switch:' . $type, $res['status']);
                    return $res['status'];
                }
                return false;
            } else {
                return $status;
            }
        } else {
            throw new \Exception('msg: no ' . $type . ' switcher');
        }

    }

    public static function changeStatus($type, $status)
    {
        $me = new self();
        $me->where('activity', $type)->update(['status' => intval($status)]);
        Cache::forget('switch:' . $type);
    }
}
