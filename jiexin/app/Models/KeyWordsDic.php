<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class KeyWordsDic extends Model
{
    protected $table = 'keywords_dic';
    protected $fillable = [
        'pid', 'c_name', 'c_name'
    ];
    private static $types = ["qiye", "jieguo", "chuangxin", "gongping"];

    public static function KeyWordsListByCate()
    {
        $data = json_decode(Cache::get('KWListByCate:all'), true);
        if (is_null($data)) {
            $tmp = KeyWordsDic::where('id', '>', 4)->get()->toArray();

            foreach ($tmp as $k => $v) {
                $data[$v['pid']][] = $v;
            }
//            dd($tmp,$data);
            Cache::put('KWListByCate:all', json_encode($data), 30);
        }
        return $data;
    }

    public static function inType($type, $kid)
    {
        foreach (self::$types as $k => $v) {
            if ($v == $type) {

                $res = self::KeyWordsListByCate()[$k + 1];
                foreach ($res as $v) {
                    if ($v['id'] == $kid) {
                        return true;
                    }
                }
                return false;
            }
        }
        return false;
    }

    public static function voteInfo($type, $uid)
    {

    }
}
