<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ValuesRanks extends Model
{
    protected $table = 'values_ranks';

    protected $fillable = [
        'uid'
    ];

    public static function addRanks($uid)
    {
        $me = new self();
        $me->uid = $uid;
        $me->save();
        return $me->id;
    }

    public static function getRanksInfo()
    {
        return self::select('users.*', 'values_ranks.id as order')->leftJoin('users', 'users.id', '=', 'values_ranks.uid')->get()->toArray();
    }
}
