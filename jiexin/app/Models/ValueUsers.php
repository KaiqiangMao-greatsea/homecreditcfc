<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class ValueUsers extends Model
{
    protected $table = 'value_users';

    protected $fillable = [
        'department', 'c_name', 'e_name', 'banner', 'title', 'phone', 'qiye', 'jieguo', 'chuangxin', 'gongping'
    ];


    public static function UserList($uid = '')
    {
        $min = 15;

        $data = Cache::get('valuesUserList:all');
        $data = json_decode($data, true);
        if (is_null($data)) {
            $data = self::orderBy('order','ASC')->get()->toArray();
            Cache::put('valuesUserList:all', json_encode($data), $min);
        }
        if ($uid) {
            foreach ($data as $v) {
                if ($v['id'] == $uid) {
                    return $v;
                }
            }
        }
        return $data;

    }

    public static function userListByDep($isCache = false)
    {
        $userList = self::UserList();
        if ($isCache) {
            $data = Cache::get('userListByDep:all');
            if (!is_null($data)) {
                return json_decode($data, true);
            }
        }
        $user = [
            "qiye" => [],
            "jieguo" => [],
            "chuangxin" => [],
            "gongping" => []
        ];
        foreach ($userList as $v) {
            if ($v['qiye']) {
                $v['count'] = Values::where('qiye', $v['id'])->count();
                $user['qiye'][] = $v;
            }

            if ($v['jieguo']) {
                $v['count'] = Values::where('jieguo', $v['id'])->count();
                $user['jieguo'][] = $v;
            }

            if ($v['chuangxin']) {
                $v['count'] = Values::where('chuangxin', $v['id'])->count();
                $user['chuangxin'][] = $v;
            }

            if ($v['gongping']) {
                $v['count'] = Values::where('gongping', $v['id'])->count();
                $user['gongping'][] = $v;
            }

        }

        $min = 1;
        Cache::put('userListByDep:all', json_encode($user), $min);
        return $user;
    }

    public static function isSameDep($judge, $playId)
    {
        $play = self::UserList($playId);
        if ($judge['department'] == $play['department']) {
            return true;
        }
        return false;
    }

    public static function getPlayerInfoById($playId)
    {
        return self::UserList($playId);
    }

    public static function inType($type, $uid)
    {
        $types = self::userListByDep()[$type];
        foreach ($types as $v) {
            if ($v['id'] == $uid) {
                return true;
            }
        }
        return false;
    }


}
