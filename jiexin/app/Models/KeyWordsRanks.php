<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KeyWordsRanks extends Model
{
    protected $table = 'keywords_ranks';

    protected $fillable = [
        'uid'
    ];

    public static function addRanks($uid)
    {
        $me = new self();
        $me->uid = $uid;
        $me->save();
        return $me->id;
    }

    public static function getRanksInfo()
    {
        return self::select('users.*', 'keywords_ranks.id as order')->leftJoin('users', 'users.id', '=', 'keywords_ranks.uid')->get()->toArray();
    }
}
