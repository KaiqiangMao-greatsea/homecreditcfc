<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Values extends Model
{
    protected $table = 'values';
    protected $fillable = [
        'uid', 'qiye', 'jieguo', 'chuangxin', 'gongping'
    ];

    public static function getVoteById($uid)
    {
        $res = self::where('uid', $uid)->get()->toArray();
        $progress = [
            "qiye" => [],
            "jieguo" => [],
            "chuangxin" => [],
            "gongping" => []
        ];
        foreach ($res as $v) {
            if ($v['qiye']) {

                $progress['qiye'][] = $v;
            }

            if ($v['jieguo']) {

                $progress['jieguo'][] = $v;
            }

            if ($v['chuangxin']) {

                $progress['chuangxin'][] = $v;
            }

            if ($v['gongping']) {
                $progress['gongping'][] = $v;
            }

        }

        return $progress;
    }

    public static function voteInfo($type, $meId)
    {
        $res = self::getVoteById($meId);
        return $res[$type];
    }

    public static function vote($me, $type, $who)
    {

        $obj = new self();
        $obj->uid = $me['id'];
        $obj->$type = $who;
        $obj->save();
        return $obj->id;

    }
}
