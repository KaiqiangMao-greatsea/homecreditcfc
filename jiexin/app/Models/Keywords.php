<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\KeyWordsDic;

class Keywords extends Model
{
    protected $fillable = [
        'uid', 'kid'
    ];

    public static function add($kid, $uid)
    {
        $me = new self();
        $me->uid = $uid;
        $me->kid = $kid;
        $me->save();

    }

    public static function getVoteById($uid)
    {
        $res = self::where('uid', $uid)->get()->toArray();
        $progress = [
            "qiye" => [],
            "jieguo" => [],
            "chuangxin" => [],
            "gongping" => []
        ];
        foreach ($res as $v) {
            $resDic = KeyWordsDic::KeyWordsListByCate();
            foreach ($resDic as $k => $vv) {
                if (self::in_Dic($v['kid'], $resDic[$k])) {
                    if ($k == 1) {//企业
                        $progress['qiye'][] = $v;
                    }
                    if ($k == 2) {//结果
                        $progress['jieguo'][] = $v;
                    }
                    if ($k == 3) {//创新
                        $progress['chuangxin'][] = $v;
                    }
                    if ($k == 4) {//企业
                        $progress['gongping'][] = $v;
                    }
                }
            }

        }

        return $progress;
    }

    private static function in_Dic($kid, $dic)
    {
        foreach ($dic as $v) {
            if ($kid == $v['id']) {
                return true;
            }
        }
        return false;
    }

    public static function voteInfo($type, $meId)
    {
        $res = self::getVoteById($meId);
        return $res[$type];
    }

    public static function vote($me, $kid)
    {
        $obj = new self();
        $obj->uid = $me['id'];
        $obj->kid = $kid;
        $obj->save();
        return $obj->id;
    }

    public static function valueAllCount($isCate = true)
    {
        if (!$isCate) {
            $group = KeyWordsDic::where('id', '>', 4)->get()->toArray();
            foreach ($group as &$v) {
                $count = Keywords::where('kid', $v['id'])->count();

                $v['count'] = $count;
            }
            return $group;
        }

        $group = Keywords::select('kid')->groupBy('kid')->get()->toArray();
        $max = 0;
        foreach ($group as $v) {
            $count = Keywords::where('kid', $v['kid'])->count();
            $countArr[$v['kid']] = $count;
            $max = $count > $max ? $count : $max;
        }

        $resDic = KeyWordsDic::KeyWordsListByCate();

        foreach ($resDic as $k => $v) {
            if ($k == 1) {//企业
                foreach ($v as $kk => $vv) {
                    $resDic[1][$kk]['count'] = $countArr[$vv['id']] ?? 0;
                }
            }
            if ($k == 2) {//结果
                foreach ($v as $kk => $vv) {
                    $resDic[2][$kk]['count'] = $countArr[$vv['id']] ?? 0;
                }
            }
            if ($k == 3) {//创新
                foreach ($v as $kk => $vv) {
                    $resDic[3][$kk]['count'] = $countArr[$vv['id']] ?? 0;
                }
            }
            if ($k == 4) {//企业
                foreach ($v as $kk => $vv) {
                    $resDic[4][$kk]['count'] = $countArr[$vv['id']] ?? 0;
                }
            }

        }
        $resDic['max'] = $max;
        return $resDic;
    }
}
