<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class SwitchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('switchs')->insert([
            'activity' => 'value',
        ]);
        DB::table('switchs')->insert([
            'activity' => 'keyword',
        ]);
    }
}
