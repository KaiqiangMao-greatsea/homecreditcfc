<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTablesKeywordsDic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keywords_dic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pid');
            $table->string('c_name');
            $table->string('e_name')->nullable();
            $table->timestamps();
            $table->index(['pid']);
        });

//        DB::table('keywords_dic')->insert([
//            ['id' => 1, 'pid' => 0, 'c_name' => '企业家精神'],
//            ['id' => 2, 'pid' => 0, 'c_name' => '结果导向'],
//            ['id' => 3, 'pid' => 0, 'c_name' => '创新精神'],
//            ['id' => 4, 'pid' => 0, 'c_name' => '公平公正']
//        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keywords_dic');
    }
}
