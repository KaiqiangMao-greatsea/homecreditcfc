<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid');
            $table->integer('qiye')->default(0);
            $table->integer('jieguo')->default(0);
            $table->integer('chuangxin')->default(0);
            $table->integer('gongping')->default(0);
            $table->timestamps();
            $table->index(['uid', 'jieguo', 'qiye', 'chuangxin', 'gongping']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('values');
    }
}
