<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable();
            $table->string('department')->nullable();
            $table->string('e_id');//员工号
            $table->string('c_name')->nullable();
            $table->string('e_name')->nullable();
            $table->tinyInteger('id_type')->default(0);//0护照 1身份证
            $table->string('id_number');//0护照 1身份证
            $table->string('title')->commit('职位');//职位
            $table->string('email')->nullable();//
            $table->string('phone')->nullable();//
            $table->tinyInteger('gender')->default(0)->commit('性别 0女');//0女 性别
            $table->tinyInteger('status')->default(0)->commit('0未激活');

            $table->rememberToken();
            $table->timestamps();
            $table->index(['department', 'e_id']);
            $table->index(['id_type', 'id_number']);
        });


//        Schema::create('users', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('type')->nullable();
//            $table->string('department')->nullable();
//            $table->string('e_id');
//            $table->string('c_name')->nullable();
//            $table->string('e_name')->nullable();
//            $table->string('title')->nullable();
//            $table->string('level')->nullable();
//            $table->string('work_location')->nullable();
//            $table->string('gender')->nullable();//0女 1男
//            $table->integer('phone')->nullable();
//            $table->string('email')->unique();
//            $table->tinyInteger('id_type')->default(0);//0护照 1身份证
//            $table->string('id_number')->nullable();//0护照 1身份证
//            $table->string('roomate')->nullable();//室友
//            $table->string('t_size')->nullable();
//            $table->string('d_city')->nullable();
//            $table->string('password');
//            $table->tinyInteger('type_id')->default(0);//0：user 1：参与活动人员
//            $table->tinyInteger('status')->default(1);
//
//            $table->rememberToken();
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
