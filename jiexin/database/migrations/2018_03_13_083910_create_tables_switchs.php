<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesSwitchs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('switchs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('activity');
            $table->tinyInteger('status')->default(0)->commit('0活动关闭，1活动开启，活动结束2');
            $table->timestamps();
            $table->index(['activity']);
        });

        DB::table('switchs')->insert([
            'activity' => 'value',
        ]);
        DB::table('switchs')->insert([
            'activity' => 'keyword',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('switchs');
    }
}
