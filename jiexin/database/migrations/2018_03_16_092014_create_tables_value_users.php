<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesValueUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('value_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('department')->nullable();
            $table->string('c_name')->nullable();
            $table->string('e_name')->nullable();
            $table->string('banner')->nullable();
            $table->string('title')->nullable();//标题
            $table->string('phone')->nullable();//
            $table->tinyInteger('qiye')->default(0);//企业家精神
            $table->tinyInteger('jieguo')->default(0);//结果为导向
            $table->tinyInteger('chuangxin')->default(0);//创新
            $table->tinyInteger('gongping')->default(0);//公平
            $table->tinyInteger('status')->default(0)->commit('0未上传banner');

            $table->rememberToken();
            $table->timestamps();
            $table->index(['department']);
            $table->index(['qiye', 'chuangxin', 'jieguo', 'gongping']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('value_users');
    }
}
