<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//
//});

Route::group(['prefix' => 'SlzgZtQxdtz'], function ($router) {
    Route::get('/admin/test', 'TestController@index');
    Route::get('/admin/maokaiqiang/test', 'TestController@reset');

    Route::get('/admin/dashboard', 'AdminController@index')->name('adminIndex');
    Route::get('/admin/switch', 'SwitchController@command')->name('switch');

    Route::post('/admin/user/add', 'UserController@userImport')->name('userAdd');
    Route::get('/admin/user/list', 'UserController@userList')->name('userList');
    Route::get('/admin/user/reset', 'UserController@usersReset')->name('userReset');

    Route::post('/admin/value/user/add', 'ValueUserController@userImport')->name('valueUserAdd');
    Route::post('/admin/value/user/banner/add', 'ValueUserController@userBanner')->name('valueBannerAdd');
    Route::get('/admin/value/user/list', 'ValueUserController@userList')->name('valueUserList');
    Route::get('/admin/value/user/dep/list', 'ValueUserController@userByDeP')->name('valueUserDepList');
    Route::get('/admin/value/user/reset', 'ValueUserController@usersReset')->name('valueUserReset');

    Route::get('/admin/keyword/list/add', 'KeywordController@AkeywordListAdd')->name('keywordListAdd');
    Route::post('/admin/keyword/list/add', 'KeywordController@keywordListAdd')->name('keywordListAdd');
    Route::get('/admin/keyword/list', 'KeywordController@keywordList')->name('keywordList');
    Route::get('/admin/keyword/reset', 'KeywordController@keywordListReset')->name('keywordListReset');



    Route::get('/admin/value/download/ranks', 'DowloadExcelController@ValueRanks')->name('excelValueRanks');
    Route::get('/admin/keyword/download/ranks', 'DowloadExcelController@KeyWordRanks')->name('excelKeywordRanks');

    Route::get('/admin/value/count', 'DowloadExcelController@ValuesCountAll')->name('excelValueCount');
    Route::get('/admin/user/value/info', 'DowloadExcelController@UserValueVoteInfo')->name('excelUserValueInfo');

    Route::get('/admin/keyword/info', 'DowloadExcelController@keywordsInfo')->name('excelKeywordsInfo');
    Route::get('/admin/value/info', 'DowloadExcelController@valuesInfo')->name('excelValuesInfo');

});

