<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group([

    'middleware' => 'auth:api',

], function ($router) {
//    Route::get('/user/value/list', 'ValueController@getAllValue');

    Route::post('/user/value/vote/{type}/{who}', 'VoteContoller@voteValue');
    Route::get('/user/value/vote/progress', 'VoteContoller@valueProgress');

    Route::post('/user/keyword/vote', 'VoteContoller@voteKeyword');
    Route::get('/user/keyword/vote/progress', 'VoteContoller@keywordProgress');

    Route::get('/user/vote/value/count', 'VoteContoller@valueCount');
    Route::get('/user/keyword/vote/count', 'VoteContoller@keywordCount');

});


Route::group([], function () {
//    value/user/dep/list', 'ValueUserController@userByDeP')->name('valueUserDepList');
    Route::get('/user/keyword/all/count', 'VoteContoller@valueAllCount');
    Route::get('/value/user/dep/list', 'ValueUserController@userByDeP');
    Route::get('/keywords/cate/list', 'KeywordController@keywordListByCate');
    Route::post('/wechat/work/config', 'WorkController@Config');

    Route::get('/switch/value/{type}', 'SwitchController@getSwitchAPI');
    Route::get('/switch/keyword/{type}', 'SwitchController@getSwitchAPI');
});
