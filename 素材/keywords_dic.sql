/*
 Navicat Premium Data Transfer

 Source Server         : Homestead
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : 192.168.10.10:3306
 Source Schema         : jiexin

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 25/03/2018 16:54:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for keywords_dic
-- ----------------------------
DROP TABLE IF EXISTS `keywords_dic`;
CREATE TABLE `keywords_dic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `c_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `keywords_dic_pid_index` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of keywords_dic
-- ----------------------------
BEGIN;
INSERT INTO `keywords_dic` VALUES (1, 0, '企业家精神', NULL, NULL, NULL);
INSERT INTO `keywords_dic` VALUES (2, 0, '结果导向', NULL, NULL, NULL);
INSERT INTO `keywords_dic` VALUES (3, 0, '创新精神', NULL, NULL, NULL);
INSERT INTO `keywords_dic` VALUES (4, 0, '公平公正', NULL, NULL, NULL);
INSERT INTO `keywords_dic` VALUES (5, 1, '视为己任', 'Responsible', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (6, 1, '全权负责', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (7, 1, '激励他人', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (8, 1, '抓住机会', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (9, 1, '不断学习', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (10, 1, '支持配合', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (11, 1, '乐于协作', 'Cooperative', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (12, 1, '传播愿景', 'vision & strategy ', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (13, 2, '实现目标', 'Delivering on targets', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (14, 2, '持续发展', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (15, 2, '客户导向', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (16, 2, '果断决策', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (17, 2, '付诸实践', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (18, 2, '遵循流程', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (19, 2, '遵守道德', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (20, 2, '拒绝投机', 'Avoiding shortcuts', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (21, 3, '参与创新', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (22, 3, '跳出陈规', 'Thinking out-of-the-box', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (23, 3, '赏识创新', 'Appreciating new ideas', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (24, 3, '开放交流', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (25, 3, '畅所欲言', 'Speaking up ', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (26, 3, '挑战现状', 'Challenging the status quo ', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (27, 3, '寻求挑战', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (28, 3, '促进变革', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (29, 4, '尊重他人', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (30, 4, '善意', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (31, 4, '温恭谦虚', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (32, 4, '正直守信', 'integrity', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (33, 4, '管理团队', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (34, 4, '寻求反馈', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (35, 4, '设身处地', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
INSERT INTO `keywords_dic` VALUES (36, 4, '一视同仁', '', '2018-03-23 15:10:40', '2018-03-23 15:10:40');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
